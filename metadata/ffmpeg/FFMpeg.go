package ffmpeg

import (
	"os/exec"
	"sonicrainboom.rocks/srberrors"
)

type FFProbeOutput struct {
	Streams []FFProbeStream `json:"streams"`
}

type FFProbeStream struct {
	Index            uint8  `json:"index"`
	CodecName        string `json:"codec_name,omitempty"`
	CodecLongName    string `json:"codec_long_name,omitempty"`
	CodecType        string `json:"codec_type,omitempty"`
	SampleFmt        string `json:"sample_fmt,omitempty"`
	SampleRate       string `json:"sample_rate,omitempty"`
	Channels         uint8  `json:"channels,omitempty"`
	ChannelLayout    string `json:"channel_layout,omitempty"`
	BitsPerSample    uint8  `json:"bits_per_sample,omitempty"`
	BitsPerRawSample string `json:"bits_per_raw_sample,omitempty"`
}

func TargetFormatByStream(stream *FFProbeStream) (format string, err error) {
	// Internally Always hash to Big-Endian
	switch stream.SampleFmt {
	case "s16":
		format = "s16be"
	case "s24":
		format = "s24be"
	case "s32":
		switch stream.BitsPerRawSample {
		case "24":
			format = "s24be"
		case "32":
			format = "s32be"
		default:
			err = srberrors.Wrap(srberrors.ErrDedupeUnsupportedDepth)
		}
	default:
		err = srberrors.Wrap(srberrors.ErrDedupeUnsupportedFormat)
	}
	return
}

func RawMetadata(location string) (buf []byte, err error) {
	cmd := exec.Command(
		"ffmpeg",
		"-i",
		location,
		"-map_metadata", "0",
		"-f",
		"ffmetadata",
		"-",
	)
	//cmd.Stderr = os.Stderr
	buf, err = cmd.Output()
	err = srberrors.Wrap(err)
	return
}
