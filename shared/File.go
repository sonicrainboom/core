package shared

import (
	"gorm.io/gorm"
	"os"
	"time"
)

type File struct {
	ID                  uint `gorm:"primarykey"`
	CreatedAt           time.Time
	UpdatedAt           time.Time
	Format              string // `pcm` for lossless PCM, `dsd` for DSD, `lossy_audio` for various lossy audio formats, 'jpg'/`png` for JPG/PNG image formats, `misc` for other/undefined
	OriginalLocation    string
	OriginalSize        int64 // Automatically set from OriginalLocation if 0
	DeduplicationBlobID uint
	DeduplicationBlob   *DeduplicationBlob `gorm:"foreignKey:DeduplicationBlobID;references:ID"`
	RawMetadataBlobID   uint
	RawMetadataBlob     *BinaryBlob `gorm:"foreignKey:DeduplicationBlobID;references:ID"`
}

func (file *File) BeforeSave(_ *gorm.DB) error {
	// Upsert size
	if file.OriginalSize == 0 {
		if stat, err2 := os.Stat(file.OriginalLocation); err2 == nil {
			file.OriginalSize = stat.Size()
		}
	}
	return nil
}
