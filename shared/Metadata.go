package shared

type Metadata struct {
	Artist *Artist
	Album  *Album
}
