package shared

import "time"

// BinaryBlob
//
// Every usage **HAS** to be implemented in `database.CleanupUnusedBinaryBlobs`, otherwise
// they will be marked as unreferenced, and cleaned up!
type BinaryBlob struct {
	ID          uint      `gorm:"primaryKey"`
	CreatedAt   time.Time `gorm:"<-:create"`
	Data        []byte    `gorm:"<-:create"`
	Sha256      string    `gorm:"<-:create;unique"`
	Compression uint8     `gorm:"<-:create;default:1"`
}

const (
	BlobCompressionUndefined = 0 // This is the default for CLI flags. It must be mapped to another value.
	BlobCompressionGzip      = 1
	BlobCompressionNone      = 255
)
