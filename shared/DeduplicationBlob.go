package shared

import (
	"gorm.io/gorm"
	"os"
	"time"
)

type DeduplicationBlob struct {
	ID                 uint      `gorm:"primaryKey"`
	CreatedAt          time.Time `gorm:"<-:create"`
	UpdatedAt          time.Time
	ContentFormat      string `gorm:"<-:create;index:idx_hash_link,unique"`
	ContentHash        string `gorm:"<-:create;index:idx_hash_link,unique"`
	IntermediateFormat string `gorm:"<-:create"`
	Location           string
	Size               int64 // Automatically set from Location if 0
}

func (blob *DeduplicationBlob) BeforeSave(_ *gorm.DB) error {
	// Upsert size
	if blob.Size == 0 {
		if stat, err2 := os.Stat(blob.Location); err2 == nil {
			blob.Size = stat.Size()
		}
	}
	return nil
}
