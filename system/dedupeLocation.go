package system

import (
	"errors"
	"log"
	"os"
	"path"
	"runtime"
	"sonicrainboom.rocks/core/shared"
	"sonicrainboom.rocks/core/system/flags"
	"sonicrainboom.rocks/srberrors"
)

const AppIdentifier = "rocks.sonicrainboom.core" // Apple App Identifier as in App Store Connect
var storageInitialized = false
var persistentStorageLocation Path = "" // Cached value after executing GetPersistentStorageLocation() if not bypassed
var ErrStorageNotInitialized = errors.New("persistent storage has not been initialized. This indicates a bug in application startup")

// GetPersistentStorageLocation builds the path to store persistent data.
//
// Because on various supported operating systems, these paths would vary, this function creates an interface.
//
// flags.DevMode causes this to return a subdirectory "dev_data" in the current working directory.
//
// If an error occurs, it is raised as a panic.
// If InitializePersistentStorage has not been called before and this
// function is called without bypassInitCheckAndCache being true, it will raise ErrStorageNotInitialized.
// Any OS-level operations (os.Getwd(), os.UserHomeDir(), etc.) might also fail and panic.
func GetPersistentStorageLocation(bypassChecksAndCache ...bool) (location Path) {
	if !storageInitialized && (len(bypassChecksAndCache) > 0 && !bypassChecksAndCache[0]) {
		panic(ErrStorageNotInitialized)
	}
	if persistentStorageLocation != "" && (len(bypassChecksAndCache) > 0 && !bypassChecksAndCache[0]) {
		return persistentStorageLocation
	}

	switch true {
	case flags.DevMode:
		cwd, err := os.Getwd()
		err = srberrors.Wrap(err)
		if err != nil {
			// This should never fail. If this panics we have bigger problems
			panic(err)
		}
		location = Path(path.Join(cwd, "dev_data"))
	case runtime.GOOS == "darwin":
		home, err := os.UserHomeDir()
		err = srberrors.Wrap(err)
		if err != nil {
			// This should never fail. If this panics we have bigger problems
			panic(err)
		}

		location = Path(path.Join(home, "Library", AppIdentifier))
	default:
		location = "/var/lib/sonicrainboom"
	}

	persistentStorageLocation = location

	return
}

// InitializePersistentStorage ensures, that the paths returned by various functions in this package are valid.
func InitializePersistentStorage() (err error) {
	if storageInitialized {
		return
	}

	defer func() {
		// This captures a potential panic thrown in GetPersistentStorageLocation
		err = srberrors.RecoverWithTrace(recover(), err)
	}()

	location := GetPersistentStorageLocation(true)

	log.Printf("Intializing storage at: %s\n", location)

	switch true {
	case runtime.GOOS == "darwin", os.Getenv("SRB_DEVMODE") != "":
		// When we are in dev-mode or we are on macOS, the directory
		// for persistent data is not created by the package manager

		err = srberrors.Wrap(location.CreateDir())
	default:
		// In other cases we assume to have the path created by the package manager.
		// But we still check.
		_, err = location.ExistsAndIsDir()
		err = srberrors.Wrap(err)
	}

	if //goland:noinspection GoNilness
	err == nil {
		// If we do NOT have an error, we're fine and set this flag
		storageInitialized = true
	}
	return
}

// GetDatabaseLocation returns the path to the SQLite database location.
//
// InitializePersistentStorage must have been called before, or a panic with ErrStorageNotInitialized is raised
func GetDatabaseLocation() Path {
	return Path(path.Join(string(GetPersistentStorageLocation()), "database.sqlite"))
}

// GetDedupeLocation returns the path to the base location of files referenced by shared.DeduplicationBlob.
//
// InitializePersistentStorage must have been called before, or a panic with ErrStorageNotInitialized is raised
func GetDedupeLocation() Path {
	return Path(path.Join(string(GetPersistentStorageLocation()), "dedupe"))
}

// GetPluginLocation returns the path to the base location of plugins to be loaded on startup.
//
// InitializePersistentStorage must have been called before, or a panic with ErrStorageNotInitialized is raised
func GetPluginLocation() Path {
	return Path(path.Join(string(GetPersistentStorageLocation()), "plugins"))
}

// GetBlobCompression returns the configured compression-level for in-database blobs.
//
// If flags.DevMode is set, this will default to shared.BlobCompressionNone, shared.BlobCompressionGzip otherwise.
func GetBlobCompression() uint8 {
	switch flags.BlobCompression {
	case shared.BlobCompressionNone, shared.BlobCompressionGzip:
		return uint8(flags.BlobCompression)
	case shared.BlobCompressionUndefined:
		if flags.DevMode {
			return shared.BlobCompressionNone
		}
		return shared.BlobCompressionGzip
	default:
		panic(srberrors.Wrap(srberrors.ErrBlobCompressionInvalid))
	}
}
