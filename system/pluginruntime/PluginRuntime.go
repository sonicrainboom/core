package pluginruntime

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/karrick/godirwalk"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"os/exec"
	"path"
	"regexp"
	"runtime"
	"sonicrainboom.rocks/core/system"
	"sonicrainboom.rocks/core/system/flags"
	"sonicrainboom.rocks/plugin/shared"
	"sonicrainboom.rocks/srberrors"
	"strconv"
	"strings"
	"time"
)

type PluginContext struct {
	info   *shared.PluginInfo
	cmd    *exec.Cmd
	port   uint16
	cookie string
}

var grpcPort uint16
var pendingRegistrations = map[string]uint16{}
var pluginRegex = regexp.MustCompile(fmt.Sprintf("(?m)srb_plugin_(%s)_(%s|universal)(\\.exe)?$", runtime.GOOS, runtime.GOARCH))
var pluginRegexFalsePlatform = regexp.MustCompile(`(?m)srb_plugin_`)

type SRBCorePluginRuntime struct {
	shared.SRBCoreServer
}

func (receiver SRBCorePluginRuntime) Announce(_ context.Context, announcement *shared.Announcement) (*shared.Acknowledgement, error) {
	if announcement.Cookie == "" {
		return nil, srberrors.New("empty cookie", srberrors.ETypePlugin)
	}
	if announcement.Port < shared.PORT_RANGE_START || announcement.Port > shared.PORT_RANGE_END {
		return nil, srberrors.New("invalid port", srberrors.ETypePlugin)
	}
	pendingRegistrations[announcement.Cookie] = uint16(announcement.Port)

	return &shared.Acknowledgement{}, nil
}

func (receiver SRBCorePluginRuntime) Ping(context.Context, *shared.Acknowledgement) (*shared.Acknowledgement, error) {
	return &shared.Acknowledgement{}, nil
}

func StartGPRC() (err error) {
	var lis net.Listener
	grpcPort = shared.PORT_RANGE_START
	for grpcPort < shared.PORT_RANGE_END {
		lis, err = net.Listen("tcp", fmt.Sprintf("127.0.0.1:%d", grpcPort))
		err = srberrors.WrapInType(err, srberrors.ETypePlugin|srberrors.ETypeNetwork|srberrors.ETypeListen)
		if err == nil {
			break
		}
		log.Println(err)
		grpcPort++
	}
	if err != nil {
		return err
	}

	log.Printf("Core gRPC listening on %s...", lis.Addr().String())
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	shared.RegisterSRBCoreServer(grpcServer, SRBCorePluginRuntime{})
	go grpcServer.Serve(lis)
	return
}

func FindPlugins(pluginSearchDir system.Path) (plugins []system.Path, err error) {
	if ok, err := pluginSearchDir.ExistsAndIsDir(); !ok {
		err = srberrors.Wrap(err)
		return nil, srberrors.WrapInType(err, srberrors.ETypePlugin|srberrors.ETypeFilesystem)
	}

	if !path.IsAbs(pluginSearchDir.String()) {
		var cwd string
		cwd, err = os.Getwd()
		err = srberrors.WrapInType(err, srberrors.ETypePlugin|srberrors.ETypeFilesystem)
		pluginSearchDir = system.Path(path.Join(cwd, pluginSearchDir.String()))
	}

	err = srberrors.Wrap(godirwalk.Walk(pluginSearchDir.String(), &godirwalk.Options{
		FollowSymbolicLinks: flags.FollowSymlinks,
		AllowNonDirectory:   true,
		ErrorCallback: func(s string, err error) godirwalk.ErrorAction {
			return godirwalk.SkipNode
		},
		Callback: func(osPathname string, de *godirwalk.Dirent) error {
			if strings.Contains(osPathname, ".git") {
				return godirwalk.SkipThis
			}
			if strings.HasPrefix(osPathname, "/Users") && strings.Contains(osPathname, "/Library/") {
				return godirwalk.SkipThis
			}
			//fmt.Printf("%s %s\n", de.ModeType(), osPathname)
			if de.IsRegular() {
				osPathname = path.Clean(osPathname)

				log.Printf("Found file %s %t\n", osPathname, pluginRegex.MatchString(osPathname))

				if pluginRegex.MatchString(osPathname) {
					plugins = append(plugins, system.Path(osPathname))
				} else if pluginRegexFalsePlatform.MatchString(osPathname) {
					log.Printf("Found plugin for other platform: '%s'. Wanting %s on %s (or universal)\n", osPathname, runtime.GOOS, runtime.GOARCH)
				}
			}

			return nil
		},
		Unsorted: true,
	}))
	return
}

func StartPlugin(timeout time.Duration, pluginPath system.Path) (info *shared.PluginInfo, err error) {
	var ok bool
	ok, err = pluginPath.ExistsAndIsFile()
	err = srberrors.WrapInType(err, srberrors.ETypePlugin)
	if !ok {
		return
	}

	if pluginRegex.MatchString(pluginPath.String()) {
		log.Printf("Loading plugin at %s...", pluginPath)
	} else if pluginRegexFalsePlatform.MatchString(pluginPath.String()) {
		log.Printf("Desired plugin is for another platform. Wanting %s on %s (or universal)", runtime.GOOS, runtime.GOARCH)
		err = srberrors.Wrap(srberrors.ErrPluginPlatformMismatch)
		return
	} else {
		err = srberrors.Wrap(srberrors.ErrPluginNoPlugin)
		return
	}

	pluginContext := PluginContext{
		info:   nil,
		cmd:    nil,
		port:   0,
		cookie: uuid.NewString(),
	}

	pluginContext.cmd = exec.Command(
		pluginPath.String(),
		"--plugin.cookie", pluginContext.cookie,
		"--core.port", strconv.Itoa(int(grpcPort)),
	)
	pluginContext.cmd.Stdout = os.Stdout
	pluginContext.cmd.Stderr = os.Stderr

	pendingRegistrations[pluginContext.cookie] = 0

	err = srberrors.WrapInType(pluginContext.cmd.Start(), srberrors.ETypePlugin|srberrors.ETypeExec)
	if err != nil {
		return nil, err
	}

	endTime := time.Now().Add(timeout)

	for time.Now().Before(endTime) {
		var port uint16
		var ok bool
		if port, ok = pendingRegistrations[pluginContext.cookie]; !ok || port == 0 {
			time.Sleep(250 * time.Millisecond)
			continue
		}

		pluginContext.port = port
	}

	delete(pendingRegistrations, pluginContext.cookie)

	if pluginContext.port == 0 {
		err = srberrors.New("plugin not registered in time", srberrors.ETypePlugin|srberrors.ETypeExec)
		if pluginContext.cmd.Process != nil {
			pluginContext.cmd.Process.Kill()
		}
		return nil, err
	}

	////////////////////

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())
	log.Printf("Connecting to plugin on port %d...\n", pluginContext.port)
	var conn *grpc.ClientConn
	ctx, _ := context.WithTimeout(context.Background(), timeout)
	conn, err = grpc.DialContext(ctx, fmt.Sprintf("127.0.0.1:%d", pluginContext.port), opts...)
	err = srberrors.WrapInType(err, srberrors.ETypePlugin|srberrors.ETypeNetwork)
	log.Printf("%#v\n", err)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	client := shared.NewCorePluginClient(conn)
	pluginContext.info, err = client.Plugin_Identify(context.Background(), &shared.PluginInfoQuery{})
	err = srberrors.WrapInType(err, srberrors.ETypePlugin|srberrors.ETypeNetwork)
	if err != nil {
		log.Fatal(err)
	}
	info = pluginContext.info
	fmt.Printf("Registered plugin: %s, version %s, developer: %s, type: %d\n", info.Name, info.Version, info.Developer, info.Type)

	_, err = client.Plugin_Initialize(context.Background(), &shared.PluginConfig{
		Strings: map[string]string{"a": "string"},
		Ints:    map[string]int32{"b": 1234},
		Bools:   map[string]bool{"c": true},
	})
	err = srberrors.WrapInType(err, srberrors.ETypePlugin|srberrors.ETypeNetwork)
	if err != nil {
		log.Fatal(err)
	}

	return
}
