package logging

import (
	"context"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"sonicrainboom.rocks/core/system/flags"
	"time"
)

type Logger struct {
}

func (l *Logger) DBLogger() *DBLogger {
	return &DBLogger{logger: l, logLevel: logger.Warn}
}

type DBLogger struct {
	logger   *Logger
	logLevel logger.LogLevel
}

func (l DBLogger) LogMode(level logger.LogLevel) logger.Interface {
	return &DBLogger{logger: l.logger, logLevel: level}
}

func (l DBLogger) Info(ctx context.Context, s string, i ...interface{}) {
	if l.logLevel >= logger.Info {
		_, _ = fmt.Fprintf(os.Stderr, s, i...)
	}
}

func (l DBLogger) Warn(ctx context.Context, s string, i ...interface{}) {
	if l.logLevel >= logger.Warn {
		_, _ = fmt.Fprintf(os.Stderr, s, i...)
	}
}

func (l DBLogger) Error(ctx context.Context, s string, i ...interface{}) {
	if l.logLevel >= logger.Error {
		_, _ = fmt.Fprintf(os.Stderr, s, i...)
	}
}

func (l DBLogger) Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowsAffected int64), err error) {
	var sql string
	var rows int64
	var elapsed time.Duration
	elapsed = time.Since(begin)
	sql, rows = fc()

	//file, line, function := srberrors.GetCaller(3) // Direct Query
	//log.Printf("%s %d %s\n", file, line, function)
	//file, line, function = srberrors.GetCaller(4) // GORM usage
	//log.Printf("%s %d %s\n", file, line, function)

	// Ignore Record not found errors here. But if we trace everything or this is a slow query, still log.
	if (err != nil && !errors.Is(err, gorm.ErrRecordNotFound)) || flags.DB.Trace || elapsed >= flags.DB.SlowLog {
		log.Printf("ctx: %+v, elapsed: %s, rows: %d, err: %+v, sql: %s\n", ctx, elapsed.String(), rows, err, sql)
	}
}
