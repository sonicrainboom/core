package system

import (
	"os"
	"path"
	"sonicrainboom.rocks/srberrors"
)

type Path string

// String returns the string representation of the Path
func (p Path) String() string {
	return string(p)
}

// Append returns a new Path, with the passed elements appended to the path.
func (p Path) Append(paths ...string) Path {
	build := []string{p.String()}
	build = append(build, paths...)
	return Path(path.Join(build...))
}

// CreateDir creates a directory (permissions 0755) at the current Path
func (p Path) CreateDir() (err error) {
	err = srberrors.Wrap(os.MkdirAll(string(p), 0755))
	return
}

// ExistsAndIsFile checks if the Path exists and is a directory.
//
// If ok is false, err contains the exact error, or os.ErrInvalid if the file exists, but is not a regular file
func (p Path) ExistsAndIsFile() (ok bool, err error) {
	var stat os.FileInfo
	stat, err = os.Stat(string(p))
	err = srberrors.WrapInType(err, srberrors.ETypeFilesystem)
	if err == nil {
		// Preserve the error. Thus we only touch `err` if there was none returned.
		if !stat.Mode().IsRegular() {
			err = srberrors.WrapInType(os.ErrInvalid, srberrors.ETypeFilesystem)
		} else {
			ok = true
		}
	}
	return
}

// ExistsAndIsDir checks if the Path exists and is a directory.
//
// If ok is false, err contains the exact error, or os.ErrInvalid if the file exists, but is not a directory
func (p Path) ExistsAndIsDir() (ok bool, err error) {
	var stat os.FileInfo
	stat, err = os.Stat(string(p))
	err = srberrors.Wrap(err)
	if err == nil {
		// Preserve the error. Thus we only touch `err` if there was none returned.
		if !stat.IsDir() {
			err = srberrors.Wrap(os.ErrInvalid)
		} else {
			ok = true
		}
	}
	return
}
