package flags

import (
	"flag"
	"os"
	"sonicrainboom.rocks/core/shared"
	"sonicrainboom.rocks/srberrors"
	"time"
)

var DevMode bool
var FollowSymlinks bool
var CustomPersistentStorage string // DO NOT USE THIS DIRECTLY! Use system.GetPersistentStorageLocation() or other system.Get*Location() functions.
var BlobCompression uint           // DO NOT USE THIS DIRECTLY! Use system.GetBlobCompression() as it maps the correct value!

type DBVars struct {
	SlowLog time.Duration
	Trace   bool
}

var DB DBVars

// This automagically makes sure this is loaded before any value is used where it may be referenced.
func init() {
	var slowLog string
	var err error
	flag.BoolVar(&DevMode, "devmode", os.Getenv("SRB_DEVMODE") != "", "When provided, devmode is enabled. (not recommended)")
	flag.BoolVar(&FollowSymlinks, "symlinks", false, "Follow symlinks when looking for media (not recommended)")
	flag.StringVar(&CustomPersistentStorage, "storage", "", "When provided, persistent storage will be stored in this directory. The path must exist.")
	flag.UintVar(&BlobCompression, "compression", shared.BlobCompressionUndefined, "When provided, use this compression-level for in-database blobs. (0 = use application default, 1 = gzip, 255 = none)")
	flag.BoolVar(&DB.Trace, "db.trace", false, "Log and trace all SQL queries (only respected when in devmode)")
	flag.StringVar(&slowLog, "db.slowlog", "500ms", "Log and trace SQL queries slower than this time")
	flag.Parse()

	DB.SlowLog, err = time.ParseDuration(slowLog)
	err = srberrors.Wrap(err)
	if err != nil {
		panic(err)
	}

	argv0 := os.Args[0]
	os.Args = append([]string{}, argv0)
	os.Args = append(os.Args, flag.Args()...)
}
