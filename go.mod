module sonicrainboom.rocks/core

go 1.16

require (
	github.com/google/uuid v1.3.0
	github.com/karrick/godirwalk v1.16.1
	google.golang.org/grpc v1.39.0
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.12
	sonicrainboom.rocks/plugin v0.0.0-20210715013005-483918400b14
	sonicrainboom.rocks/srberrors v0.0.0-20210715011724-21e559bff791
)
