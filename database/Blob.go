package database

import (
	"bytes"
	"compress/gzip"
	"crypto/sha256"
	"fmt"
	"sonicrainboom.rocks/core/shared"
	"sonicrainboom.rocks/core/system"
	"sonicrainboom.rocks/srberrors"
)

func (db *DB) SaveBlob(data []byte) (id uint, err error) {
	hash := sha256.New()
	hash.Write(data)

	var blob shared.BinaryBlob
	blob.Sha256 = fmt.Sprintf("%x", hash.Sum(nil))

	_ = db.First(&blob, &blob).Error
	if blob.ID != 0 {
		// We found one
		id = blob.ID
		return
	}

	switch system.GetBlobCompression() {
	case shared.BlobCompressionGzip:
		var b bytes.Buffer
		gz := gzip.NewWriter(&b)
		if _, err = gz.Write(data); err != nil {
			err = srberrors.Wrap(err)
			return
		}
		if err = srberrors.Wrap(gz.Close()); err != nil {
			return
		}
		blob.Data = b.Bytes()
		blob.Compression = shared.BlobCompressionGzip
	case shared.BlobCompressionNone:
		fallthrough
	default:
		blob.Data = data
		blob.Compression = shared.BlobCompressionNone
	}

	err = srberrors.Wrap(db.Create(&blob).Error)
	id = blob.ID

	return
}

func (db *DB) GetBlob(id uint) (data []byte, err error) {
	var blob shared.BinaryBlob
	err = db.First(&blob, id).Error

	switch blob.Compression {
	case shared.BlobCompressionGzip:
		var gz *gzip.Reader
		if gz, err = gzip.NewReader(bytes.NewReader(blob.Data)); err != nil {
			err = srberrors.Wrap(err)
			return
		}
		if _, err = gz.Read(data); err != nil {
			data = nil
			err = srberrors.Wrap(err)
			return
		}
		if err = srberrors.Wrap(gz.Close()); err != nil {
			data = nil
			return
		}
	case shared.BlobCompressionNone:
		data = blob.Data
	default:
		err = srberrors.Wrap(srberrors.ErrBlobCompressionInvalid)
		return
	}

	return
}
