package database

import (
	"context"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"sonicrainboom.rocks/core/system/logging"
	"sonicrainboom.rocks/srberrors"
	"time"
)

type DB struct {
	*gorm.DB
	*time.Ticker
	isSubInstance bool
}

func (db *DB) WithContext(ctx context.Context) *DB {
	return &DB{DB: db.DB.WithContext(ctx), Ticker: nil, isSubInstance: true}
}

func (db *DB) IsValid() bool {
	if db.DB != nil {
		return true
	}
	return false
}

func (db *DB) Close() {
	if db.isSubInstance {
		// Don't close when we are an instance created via `WithContext`.
		return
	}
	if !db.IsValid() {
		return
	}
	_ = db.Maintenance(true)
	db.DB = nil
}

func (db *DB) Maintenance(mayBlockReads bool) (err error) {
	if !db.IsValid() {
		return
	}

	err = srberrors.Wrap(db.Exec("PRAGMA quick_check;").Error)
	if err != nil {
		return
	}

	err = srberrors.Wrap(db.Exec("PRAGMA optimize;").Error)
	if err != nil {
		return
	}

	err = srberrors.Wrap(db.Exec("ANALYZE;").Error)
	if err != nil {
		return
	}

	err = srberrors.Wrap(db.Debug().Exec("VACUUM;").Error)
	if err != nil {
		return
	}

	if mayBlockReads {
		// This mode works the same way as RESTART with the addition that the WAL file is truncated to zero
		// bytes upon successful completion.
		err = srberrors.Wrap(db.Exec("PRAGMA wal_checkpoint(TRUNCATE);").Error)
		if err != nil {
			return
		}
	} else {
		// FULL blocks concurrent writers while it is running, but readers can proceed.
		err = srberrors.Wrap(db.Exec("PRAGMA wal_checkpoint(FULL);").Error)
		if err != nil {
			return
		}
	}

	err = srberrors.Wrap(db.Exec("PRAGMA shrink_memory;").Error)
	if err != nil {
		return
	}

	return
}

func Open(filename string, wal bool, autoMaintenance bool) (db *DB, err error) {
	db = &DB{
		DB: nil,
	}
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Warn, // Log level
			IgnoreRecordNotFoundError: false,       // Ignore ErrRecordNotFound error for logger
			Colorful:                  true,        // Disable color
		},
	)

	srbLogger := logging.Logger{}
	_ = newLogger
	_ = srbLogger

	db.DB, err = gorm.Open(sqlite.Open(filename), &gorm.Config{
		//Logger:                                   newLogger,
		Logger:  srbLogger.DBLogger(),
		NowFunc: time.Now,
	})
	err = srberrors.Wrap(err)
	if err != nil {
		return nil, err
	}

	// Set application_id to ASCII 'srbc'
	err = srberrors.Wrap(db.Exec("PRAGMA application_id=0x73726263;").Error)
	if err != nil {
		return nil, err
	}
	if wal {
		err = srberrors.Wrap(db.Exec("PRAGMA journal_mode=WAL;").Error)
		if err != nil {
			return nil, err
		}

		err = srberrors.Wrap(db.Exec("PRAGMA journal_size_limit=512000000;").Error)
		if err != nil {
			return nil, err
		}
	}

	if autoMaintenance {
		err = srberrors.Wrap(db.Maintenance(true))
		db.Ticker = time.NewTicker(5 * time.Minute)
		go func(db *DB) {
			for {
				<-db.C
				_ = db.Maintenance(false)
			}
		}(db)
	}

	return
}
