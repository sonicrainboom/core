package database

import (
	"database/sql"
	"log"
	"os"
	"sonicrainboom.rocks/core/shared"
	"sonicrainboom.rocks/srberrors"
)

func panicIfError(err error) {
	if err != nil {
		err = srberrors.NestedWrap(err, 1, "panic")
		panic(err)
	}
}

func (db *DB) CleanupUnusedDeduplicationBlobs() (affected uint, err error) {
	referenced := make(map[uint]bool)
	paths := make(map[uint]string)
	var rows *sql.Rows
	tx := db.Begin(&sql.TxOptions{Isolation: sql.LevelSerializable})
	defer func() {
		if err = srberrors.RecoverWithTrace(recover(), err); err != nil {
			tx.Rollback()
		}
	}()

	// region existingBlobs
	rows = nil
	query := tx.Model(&shared.DeduplicationBlob{}).Select("id, location")
	panicIfError(query.Error)
	rows, err = query.Rows()
	panicIfError(err)
	var id uint
	var path string
	for rows.Next() {
		err = rows.Scan(&id, &path)
		if err != nil {
			panicIfError(err)
		}
		referenced[id] = false
		paths[id] = path
		id = 0
		path = ""
	}
	//endregion

	// region File.RawMetadataBlob
	rows = nil
	metadata := tx.Model(&shared.File{}).Distinct("deduplication_blob_id")
	panicIfError(metadata.Error)
	rows, err = metadata.Rows()
	panicIfError(err)
	for rows.Next() {
		err = rows.Scan(&id)
		if err != nil {
			panicIfError(err)
		}
		referenced[id] = true
		id = 0
	}
	panicIfError(err)
	//endregion

	// []interface{} so we can pass it to .Delete()
	var unreferenced []interface{}
	for k, v := range referenced {
		if !v {
			unreferenced = append(unreferenced, k)
			if paths[k] != "" {
				err = os.Remove(paths[k])
				panicIfError(err)
			}
		}
	}

	log.Printf("Found %d unreferenced deduplication blobs\n", len(unreferenced))

	if len(unreferenced) > 0 {
		err = tx.Model(&shared.DeduplicationBlob{}).Delete(&shared.DeduplicationBlob{}, unreferenced...).Error
		panicIfError(err)
	}

	err = srberrors.Wrap(tx.Commit().Error)
	return
}
func (db *DB) CleanupUnusedBinaryBlobs() (affected uint, err error) {
	referenced := make(map[uint]bool)
	var rows *sql.Rows
	var id uint
	tx := db.Begin(&sql.TxOptions{Isolation: sql.LevelSerializable})
	defer func() {
		if err = srberrors.RecoverWithTrace(recover(), err); err != nil {
			tx.Rollback()
		}
	}()

	// region existingBlobs
	rows = nil
	query := tx.Model(&shared.BinaryBlob{}).Distinct("id")
	panicIfError(query.Error)
	rows, err = query.Rows()
	panicIfError(err)
	for rows.Next() {
		err = rows.Scan(&id)
		if err != nil {
			panicIfError(err)
		}
		referenced[id] = false
		id = 0
	}
	panicIfError(err)
	//endregion

	// region File.RawMetadataBlob
	rows = nil
	metadata := tx.Model(&shared.File{}).Distinct("raw_metadata_blob_id")
	panicIfError(metadata.Error)
	rows, err = metadata.Rows()
	panicIfError(err)
	for rows.Next() {
		err = rows.Scan(&id)
		if err != nil {
			panicIfError(err)
		}
		referenced[id] = true
		id = 0
	}
	panicIfError(err)
	//endregion

	// []interface{} so we can pass it to .Delete()
	var unreferenced []interface{}
	for k, v := range referenced {
		if !v {
			unreferenced = append(unreferenced, k)
		}
	}

	log.Printf("Found %d unreferenced binary blobs\n", len(unreferenced))

	if len(unreferenced) > 0 {
		err = tx.Model(&shared.BinaryBlob{}).Delete(&shared.BinaryBlob{}, unreferenced...).Error
		panicIfError(err)
	}

	err = srberrors.Wrap(tx.Commit().Error)
	return
}
