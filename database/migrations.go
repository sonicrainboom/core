package database

import (
	"gorm.io/gorm"
	"sonicrainboom.rocks/core/shared"
	"sonicrainboom.rocks/srberrors"
)

// RefreshModels
//
// This can be used to refresh data, that is only generated on save (such as sizes for files)
func (db *DB) RefreshModels() (err error) {
	err = srberrors.Wrap(db.refresh(&[]shared.DeduplicationBlob{}))
	if err != nil {
		return
	}
	err = srberrors.Wrap(db.refresh(&[]shared.BinaryBlob{}))
	if err != nil {
		return
	}
	err = srberrors.Wrap(db.refresh(&[]shared.File{}))
	if err != nil {
		return
	}
	err = srberrors.Wrap(db.refresh(&[]shared.SongVersion{}))
	if err != nil {
		return
	}
	return
}

func (db *DB) refresh(modelSliceRef interface{}) (err error) {
	err = srberrors.Wrap(db.FindInBatches(modelSliceRef, 100, func(tx *gorm.DB, batch int) (err error) {
		return srberrors.Wrap(tx.Save(modelSliceRef).Error)
	}).Error)
	return
}

// MigrateModels
//
// This will migrate models to the current version
func (db *DB) MigrateModels() (err error) {
	return srberrors.Wrap(db.AutoMigrate(
		&shared.BinaryBlob{},
		&shared.DeduplicationBlob{},
		&shared.File{},
		&shared.SongVersion{},
	))
}
