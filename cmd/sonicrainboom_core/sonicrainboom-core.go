package main

import (
	"errors"
	"fmt"
	"github.com/karrick/godirwalk"
	"log"
	"os"
	"regexp"
	"runtime"
	"sonicrainboom.rocks/core/contentmanagement/deduplication"
	"sonicrainboom.rocks/core/database"
	"sonicrainboom.rocks/core/system"
	"sonicrainboom.rocks/core/system/flags"
	"sonicrainboom.rocks/core/system/pluginruntime"
	"sonicrainboom.rocks/plugin/shared"
	"sonicrainboom.rocks/srberrors"
	"strings"
	"sync"
	"time"
)

func printLicense() {
	_, _ = fmt.Fprintf(os.Stderr, "SonicRainBoom Core - Copyright (C) 2021 Hendrik 'T4cC0re' Meyer\nhttps://sonicrainboom.rocks/\n\n")
}

func printSystem() {
	_, _ = fmt.Fprintf(os.Stderr, "System:\t%s/%s/%s\n\n", runtime.GOOS, runtime.GOARCH, runtime.Version())
}

func main() {
	var db *database.DB
	var err error
	defer func() {
		if err = srberrors.RecoverWithTrace(recover(), err); err != nil {
			panic(err)
		}
	}()
	printLicense()
	printSystem()
	err = srberrors.Wrap(system.InitializePersistentStorage())
	if err != nil {
		log.Fatalf("Fatal: %+v\n", err)
	}

	db, err = database.Open(system.GetDatabaseLocation().String(), true, true)
	err = srberrors.Wrap(err)
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()
	err = srberrors.Wrap(db.MigrateModels())
	if err != nil {
		log.Fatal(err)
	}

	log.Println(deduplication.FileDedupeStats(db))
	log.Println(db.CleanupUnusedBinaryBlobs())
	log.Println(db.CleanupUnusedDeduplicationBlobs())
	//err = srberrors.Wrap(db.RefreshModels())
	//if err != nil {
	//	log.Fatal(err)
	//}

	// region plugins
	err = srberrors.Wrap(pluginruntime.StartGPRC())
	if err != nil {
		log.Fatal(err)
	}

	var plugins []system.Path
	plugins, err = pluginruntime.FindPlugins(system.GetPluginLocation())
	err = srberrors.Wrap(err)
	// Ignore any errors in relation to the Filesystem
	if err != nil && !srberrors.IsAnyType(err, srberrors.ETypeFilesystem) {
		log.Fatal(err)
	} else {
		err = nil
	}

	var info *shared.PluginInfo
	for _, plugin := range plugins {
		info, err = pluginruntime.StartPlugin(time.Second, plugin)
		err = srberrors.Wrap(err)
		if err != nil {
			log.Fatal(err)
		}
	}

	_ = info
	// end region

	if len(os.Args) < 2 {
		return
	}

	regex := regexp.MustCompile(`(?m)(\.aiff?|\.flac|\.alac)$`)

	fileChan := make(chan string, 1)
	errorChan := make(chan error, 0)

	group := sync.WaitGroup{}
	go func() {
		for {
			select {
			case err := <-errorChan:
				if err != nil {
					log.Fatalf("Flags: %v, Err: %v", srberrors.CombinedErrorType(err), srberrors.Wrap(err))
				}
			}
		}
	}()

	for i := 0; i < runtime.GOMAXPROCS(0); i++ {
		go func(i int) {
			log.Printf("Started worker %d\n", i)
			for {
				select {
				case osPathname := <-fileChan:
					err := srberrors.Wrap(deduplication.AddPCMFile(db, osPathname))
					if errors.Is(err, os.ErrNotExist) {
						// Ignore if the file does not exist
						group.Done()
					}
					if err != nil && (strings.Contains(err.Error(), "UNIQUE constraint failed:") || strings.Contains(err.Error(), "database is locked")) {
						// retry on data-daces
						fileChan <- osPathname
					} else {
						if err != nil {
							errorChan <- err
						}
						group.Done()
					}
				}
			}
		}(i)
	}

	for _, v := range os.Args[1:] {
		errorChan <- srberrors.Wrap(godirwalk.Walk(v, &godirwalk.Options{
			FollowSymbolicLinks: flags.FollowSymlinks,
			AllowNonDirectory:   true,
			ErrorCallback: func(s string, err error) godirwalk.ErrorAction {
				log.Printf("Walk (%s): ErrorCallback: %s: %v\n", v, s, err)
				return godirwalk.SkipNode
			},
			Callback: func(osPathname string, de *godirwalk.Dirent) error {
				// Following string operation is not most performant way
				// of doing this, but common enough to warrant a simple
				// example here:
				if strings.Contains(osPathname, ".git") {
					return godirwalk.SkipThis
				}
				if strings.HasPrefix(osPathname, "/Users") && strings.Contains(osPathname, "/Library/") {
					return godirwalk.SkipThis
				}
				//fmt.Printf("%s %s\n", de.ModeType(), osPathname)
				if de.IsRegular() {
					if regex.MatchString(osPathname) {
						group.Add(1)
						fileChan <- osPathname
					}
				}

				return nil
			},
			Unsorted: true,
		}))
	}

	group.Wait()
}
