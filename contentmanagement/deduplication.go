package contentmanagement

import (
	"errors"
	"os"
)

// system.Path's ExistsAndIsFile() method should be used instead
// @deprecated
func FileExists(path string) bool {
	if stat, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
		return false
	} else {
		return !stat.IsDir()
	}
}
