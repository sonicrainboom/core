package deduplication

import (
	"errors"
	"fmt"
	"gorm.io/gorm"
	"sonicrainboom.rocks/core/contentmanagement"
	"sonicrainboom.rocks/core/database"
	"sonicrainboom.rocks/core/metadata/ffmpeg"
	"sonicrainboom.rocks/core/shared"
	"sonicrainboom.rocks/srberrors"
)

type Context struct {
	Hash           string
	HashFormat     string
	SourceLocation string
	Context        interface{}
}

func FileDedupeStats(db *database.DB) (files, blobs uint, err error) {
	tx := db.Model(&shared.File{}).Select(`COUNT(id) as files, COUNT(DISTINCT deduplication_blob_id) as blobs`)
	if tx.Error != nil {
		err = srberrors.Wrap(tx.Error)
		return
	}
	err = srberrors.Wrap(tx.Row().Scan(&files, &blobs))
	return
}

func DeduplicateFile(db *database.DB, fileId uint) (err error) {
	var file shared.File
	if err = srberrors.Wrap(db.Preload("DeduplicationBlob").First(&file, fileId).Error); err != nil {
		return
	}

	if file.DeduplicationBlob != nil && contentmanagement.FileExists(file.DeduplicationBlob.Location) {
		fmt.Printf("Using deduplication blob(%s): %s\n", file.DeduplicationBlob.ContentFormat, file.DeduplicationBlob.ContentHash)
		// There is a dedupe entry
	} else {
		var dedupe *Context

		switch file.Format {
		case "pcm":
			dedupe, err = InitPCMDeduplication(file.OriginalLocation)
			err = srberrors.Wrap(err)
		case "jpg":
		case "png":
		default:
			err = srberrors.Wrap(srberrors.ErrDedupeUnsupportedType)
		}
		if err != nil {
			return
		}

		var blob shared.DeduplicationBlob
		err = db.First(&blob, shared.DeduplicationBlob{ContentFormat: dedupe.HashFormat, ContentHash: dedupe.Hash}).Error
		if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
			err = srberrors.Wrap(err)
			return
		}
		if blob.ID > 0 && contentmanagement.FileExists(blob.Location) {
			fmt.Printf("Found deduplication blob(%s): %s\n", blob.ContentFormat, blob.ContentHash)
			file.DeduplicationBlob = &blob
		} else {
			fmt.Printf("No deduplication blob found. Creating...\n")

			// We need to dedupe, as there is no matching record
			var newLocation string
			var dedupeFormat string
			newLocation, dedupeFormat, err = dedupe.Perform()
			err = srberrors.Wrap(err)
			if err != nil {
				return
			}

			blob.ContentFormat = dedupe.HashFormat
			blob.ContentHash = dedupe.Hash
			blob.IntermediateFormat = dedupeFormat
			blob.Location = newLocation

			err = srberrors.Wrap(db.Save(&blob).Error)
			if err != nil {
				return
			}
			fmt.Printf("Created deduplication blob(%s): %s\n", blob.ContentFormat, blob.ContentHash)
			file.DeduplicationBlob = &blob
		}
		err = srberrors.Wrap(db.Save(&file).Error)
	}

	return
}

// Perform
//
// Creates a new resource to be referenced as a neutral copy without attached metadata
//
// It will also check, that any new representation of the resource will match the input
func (dedupe *Context) Perform() (newLocation string, dedupeFormat string, err error) {
	switch dedupe.Context.(type) {
	case *ffmpeg.FFProbeStream:
		newLocation, dedupeFormat, err = PCMCompressWithFLAC(dedupe, dedupe.Context.(*ffmpeg.FFProbeStream))
		err = srberrors.Wrap(err)
		if err != nil {
			return
		}
		var check *Context
		check, err = InitPCMDeduplication(newLocation)
		err = srberrors.Wrap(err)
		if err != nil {
			return
		}
		if check.Hash != dedupe.Hash || check.HashFormat != dedupe.HashFormat {
			err = srberrors.Wrap(srberrors.ErrDedupeResultMismatch, dedupe.SourceLocation)
		}
	default:
		err = srberrors.Wrap(srberrors.ErrDedupeCorruptContext)
	}
	return
}
