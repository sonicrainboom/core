package deduplication

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"sonicrainboom.rocks/core/database"
	"sonicrainboom.rocks/core/metadata/ffmpeg"
	"sonicrainboom.rocks/core/shared"
	"sonicrainboom.rocks/core/system"
	"sonicrainboom.rocks/srberrors"
	"strconv"
)

func PCMCompressWithFLAC(dedupe *Context, stream *ffmpeg.FFProbeStream) (newLocation string, dedupeFormat string, err error) {
	dirname := system.GetDedupeLocation().Append(dedupe.Hash[0:2], dedupe.Hash[2:5])
	err = srberrors.Wrap(dirname.CreateDir())
	if err != nil {
		return
	}
	_, err = dirname.ExistsAndIsDir()
	err = srberrors.Wrap(err)
	if err != nil {
		return
	}

	filename := path.Join(dirname.String(), fmt.Sprintf("%s.%s.flac", dedupe.Hash, dedupe.HashFormat))

	cmd := exec.Command(
		"ffmpeg",
		"-i", dedupe.SourceLocation,
		"-ar", stream.SampleRate,
		"-ac", strconv.Itoa(int(stream.Channels)),
		"-compression_level", "10",
		"-map_metadata", "-1",
		"-vn",
		"-sn",
		"-fflags", "+bitexact",
		"-flags:a", "+bitexact",
		"-y",
		"-f", "flac",
		filename,
	)
	//cmd.Stderr = os.Stderr
	err = srberrors.WrapInType(cmd.Run(), srberrors.ETypeDeduplication|srberrors.ETypeFileFormat|srberrors.ETypeFFMpeg|srberrors.ETypeExec)
	if err != nil {
		_ = os.Remove(newLocation)
		return
	}

	return filename, "flac", nil
}

func PCMGetRawHash(location string, stream *ffmpeg.FFProbeStream) (contentHash string, hashFormat string, err error) {
	var format string
	format, err = ffmpeg.TargetFormatByStream(stream)
	err = srberrors.Wrap(err)
	if err != nil {
		return
	}

	hash := sha256.New()

	cmd := exec.Command(
		"ffmpeg",
		"-i",
		location,
		"-ar",
		stream.SampleRate,
		"-ac",
		strconv.Itoa(int(stream.Channels)),
		"-map_metadata", "-1",
		"-vn",
		"-sn",
		"-fflags", "+bitexact",
		"-flags:a", "+bitexact",
		"-c:a",
		fmt.Sprintf("pcm_%s", format),
		"-f",
		format,
		"-",
	)
	//cmd.Stderr = os.Stderr
	cmd.Stdout = hash

	err = srberrors.WrapInType(cmd.Run(), srberrors.ETypeDeduplication|srberrors.ETypeFileFormat|srberrors.ETypeFFMpeg|srberrors.ETypeExec)
	if err != nil {
		return
	}

	tmp := hash.Sum(nil)

	return fmt.Sprintf("%x", tmp), fmt.Sprintf("pcm_%s_%s", format, stream.SampleRate), nil
}

func PCMGetStream(location string) (stream *ffmpeg.FFProbeStream, err error) {
	cmd := exec.Command(
		"ffprobe",
		"-i",
		location,
		"-print_format",
		"json",
		"-show_streams",
	)

	//cmd.Stderr = os.Stderr
	var out []byte
	out, err = cmd.Output()
	err = srberrors.Wrap(err)
	if err != nil {
		return
	}

	var response ffmpeg.FFProbeOutput
	err = srberrors.Wrap(json.NewDecoder(bytes.NewReader(out)).Decode(&response))
	if err != nil {
		return
	}

	var _stream ffmpeg.FFProbeStream
	for _, v := range response.Streams {
		if v.CodecType != "audio" {
			continue
		}
		switch v.BitsPerRawSample {
		case "16", "24", "32":
			_stream = v
			break
		case "":
			switch v.BitsPerSample {
			case 16, 24, 32:
				v.BitsPerRawSample = strconv.Itoa(int(v.BitsPerSample))
				_stream = v
			default:
				fmt.Printf("%+v\n", v)
				err = srberrors.Wrap(srberrors.ErrDedupeUnsupportedDepth)
			}
		default:
			fmt.Printf("%+v\n", v)
			err = srberrors.Wrap(srberrors.ErrDedupeUnsupportedDepth)
		}
	}

	if _stream.CodecType == "" {
		err = srberrors.Wrap(srberrors.ErrDedupeNoAudioStream)
	} else {
		stream = &_stream
	}

	return
}

func AddPCMFile(db *database.DB, path string) (err error) {
	file := shared.File{
		Format:           "pcm",
		OriginalLocation: path,
	}
	err = srberrors.Wrap(db.FirstOrCreate(&file, &file).Error)
	if err != nil {
		return
	}

	if file.RawMetadataBlobID == 0 {
		var buf []byte
		buf, err = ffmpeg.RawMetadata(path)
		err = srberrors.Wrap(err)
		if err != nil {
			return
		}
		file.RawMetadataBlobID, err = db.SaveBlob(buf)
		err = srberrors.Wrap(err)
		if err != nil {
			return
		}
		err = srberrors.Wrap(db.Save(&file).Error)
		if err != nil {
			return
		}
	}

	err = srberrors.Wrap(DeduplicateFile(db, file.ID))
	if srberrors.IsAnyType(err, srberrors.ETypeDeduplication|srberrors.ETypeFileFormat) {
		// File-level dedupe errors. Ignore them.
		log.Printf("Flags: %v, File dedupe error: %v\n", srberrors.CombinedErrorType(err), err)
		err = nil
	}
	return
}

func InitPCMDeduplication(location string) (dedupe *Context, err error) {
	var stream *ffmpeg.FFProbeStream
	stream, err = PCMGetStream(location)
	err = srberrors.Wrap(err)
	if err != nil {
		return
	}

	var contentHash string
	var hashFormat string
	contentHash, hashFormat, err = PCMGetRawHash(location, stream)
	err = srberrors.Wrap(err)
	if err != nil {
		return
	}

	fmt.Printf("hash(%s): %s\n", hashFormat, contentHash)

	dedupe = &Context{
		HashFormat:     hashFormat,
		Hash:           contentHash,
		SourceLocation: location,
		Context:        stream,
	}

	return
}
